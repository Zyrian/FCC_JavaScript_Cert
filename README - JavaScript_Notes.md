 # JavaScript

JavaScript notes from Free Code Camp lessons.

## Accessing Multi-Dimensional Arrays

A Multi-Dimensional array = array of arrays. 
* First bracket refers to entries on the first level.
* Each additional bracket refers to the next level of entries.

Example:
```
var arr = [
  [1,2,3],
  [4,5,6],
  [7,8,9],
  [[10,11,12], 13, 14]
];
arr[3]; // equals [[10,11,12], 13, 14]
arr[3][0]; // equals [10,11,12]
arr[3][0][1]; // equals 11
```

### Switch Statements

* Used when you have many options to choose from.
* Statements are executed from the first matched *case* until a *break* is encountered.
* Can be used instead of if/else statements if there are many options to choose from.

Example:
```
function chainToSwitch(val) {
  var answer = "";
  switch(val) {
    case "bob":
      answer = "Marley";
      break;
    case 42:
      answer = "The Answer";
      break;
    case 1:
      answer = "There is no #1";
      break;
    case 99:
      answer = "Missed me by this much!";
      break;
    case 7:
      answer = "Ate Nine";
      break;  
    default:
      answer = "";
  }  
  return answer;  
}
```

**Counting Cards**

Using both switch and if/else statements:
```
function cc(card) {
  switch(card) {
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
      count++;
      break;
    case 7:
    case 8:
    case 9:
      break;
    case 10:
    case 'J':
    case 'Q':
    case 'K':
    case 'A':
      count--;
      break;
  }

  var answer = count;
  if (count <= 0) {
    answer += " Hold";
  } else {
    answer += " Bet";
  }
  
  return answer;
}
```

#### JavaScript Objects

Similar to *arrays*, but uses *properties* instead of indexes when accessing and/or modifying object data.

**Example:**
```
var cat = {
  name: "Whiskers",
  legs: 4,
  tails: 1,
  "enemies": ["Water", "Dogs"]
};
```
Quotes can be removed from single-word string properties, but JS automatically type-casts nonstring properties into strings.

**Accessing Nested Objects**

Subproperties of objects can be accessed by chaining dot and/or brackets together.

**Example:**
```
var ourStorage = {
  "desk": {
    "drawer": "stapler"
  },
  "cabinet": {
    "top drawer": {
      "folder1": "a file",
      "folder2": "secrets"
    },
    "bottom drawer": "soda"
  }
};
ourStorage.cabinet["top drawer"].folder2; // "secrets"
ourStorage.desk.drawer; // "stapler"
```